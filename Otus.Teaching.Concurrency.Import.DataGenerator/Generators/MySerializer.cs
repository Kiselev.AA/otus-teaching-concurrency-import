﻿using System.Reflection;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
    public class MySerializer<T> where T : class, new()
    {
        private readonly BindingFlags allFlags;
        private List<FieldInfo> _fields;

        public MySerializer()
        {
            long flags = 0;
            for (int i = 0; i < 25; i++)
            {
                flags += (long)Math.Pow(2, i);
            }
            allFlags = (BindingFlags)flags;

            var type = typeof(T);
            _fields = type.GetFields(allFlags).ToList();
        }

        public string Serialize(T obj)
        {
            var type = typeof(T);

            string[] resultNames = new string[_fields.Count];
            string[] resultValues = new string[_fields.Count];

            for (int i = 0; i < _fields.Count; i++)
            {
                resultNames[i] = _fields[i].Name;
                resultValues[i] = type.GetField(name: _fields[i].Name, bindingAttr: allFlags).GetValue(obj).ToString();
            }

            return string.Join(',', resultNames) + '\n' + string.Join(',', resultValues);
        }

        public string Serialize(List<T> listObj)
        {
            var type = typeof(T);

            string[] resultNames = new string[_fields.Count];
            string[] tempResultValues = new string[_fields.Count];
            string[] resultValues = new string[listObj.Count];
            for (int j = 0; j < listObj.Count; j++)
            {
                for (int i = 0; i < _fields.Count; i++)
                {
                    resultNames[i] = _fields[i].Name;
                    tempResultValues[i] = type.GetField(name: _fields[i].Name, bindingAttr: allFlags).GetValue(listObj[j]).ToString();
                }
                resultValues[j] = string.Join(',', tempResultValues);
            }

            return string.Join(',', resultNames) + '\n' + string.Join('\n', resultValues);
        }

        public T DeSerialize(string csv)
        {
            var result = new T();
            string[] csvArr = csv.Split('\n');
            string[] names = csvArr[0].Split(',');
            string[] values = csvArr[1].Split(',');

            for (int i = 0; i < _fields.Count; i++)
            {
                var value = values[i];
                var name = names[i];
                var field = _fields.First(f => f.Name == name);

                var converter = TypeDescriptor.GetConverter(_fields[0].FieldType);
                var convertedvalue = converter.ConvertFrom(value);

                field.SetValue(result, convertedvalue);
            }

            return result;
        }

        public List<T> DeSerialize(List<string> csv)
        {
            var resultList = new List<T>();
            string[] names = csv[0].Split(',');
            //string[,] values = new string[csv.Count - 1, names.Length];
            for (int j = 0; j < csv.Count - 1; j++)
            {
                var temporaryData = csv[j+1].Split(',');
                T element = new T();
                for (int i = 0; i < names.Length; i++)
                {
                    //values[j, i] = temporaryData[i];
                    string value = temporaryData[i];
                    var name = names[i];
                    var field = _fields.First(f => f.Name == name);

                    var converter = TypeDescriptor.GetConverter(_fields[i].FieldType);
                    var convertedvalue = converter.ConvertFrom(value);

                    field.SetValue(element, convertedvalue);
                }
                resultList.Add(element);
            }

            return resultList;
        }
    }
}
