using System;
using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import;
using Microsoft.EntityFrameworkCore;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        private MyDbContext _context;
        protected DbSet<Customer> _dbSet;

        public CustomerRepository(MyDbContext context)
        {
            _context = context;
            _dbSet = context.Set<Customer>();
        }
        public void AddCustomer(Customer customer)
        {
            if (customer != null)
                _dbSet.Add(customer);
            //SaveChange();
        }

        public void Clear()
        {
            _dbSet.RemoveRange(_dbSet);
            SaveChange();
        }

        public void SaveChange()
        {
            _context.SaveChanges();
        }
    }
}