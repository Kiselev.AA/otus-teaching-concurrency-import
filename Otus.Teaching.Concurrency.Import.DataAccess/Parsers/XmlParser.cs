﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
//using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Data;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser : IDataParser<List<Customer>>
    {
        public string DataFile { get; set; }

        public XmlParser(string dataFile)
        {
            DataFile = dataFile;
        }

        public List<Customer> Parse()
        {
            var result = new List<Customer>();

            var serializer = new XmlSerializer(typeof(CustomersList));

            using (FileStream fileStream = new FileStream(DataFile, FileMode.OpenOrCreate, FileAccess.Read, FileShare.Read))
            {
                CustomersList? item = (CustomersList)serializer.Deserialize(fileStream);
                result=item.Customers;
            }

            return result;
        }
    }
}