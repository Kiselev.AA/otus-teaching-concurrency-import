﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvParser : IDataParser<List<Customer>>
    {
        public List<string> DataToParse { get; set; }

        public CsvParser() { }
        public CsvParser(List<string> dataToParse)
        {
            DataToParse = dataToParse;
        }

        public List<Customer> Parse()
        {
            var serializer = new MySerializer<Customer>();
            return serializer.DeSerialize(DataToParse);
        }

        public List<string> ReadFile(string dataFile)
        {
            var csv = new List<string>();

            using (var reader = new StreamReader(dataFile))
            {
                string? line = "";
                while ((line = reader.ReadLine()) != null)
                    csv.Add(line);
            }
            return csv;
        }
    }
}
