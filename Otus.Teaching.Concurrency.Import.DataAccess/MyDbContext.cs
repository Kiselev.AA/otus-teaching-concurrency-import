﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess
{
    public class MyDbContext : DbContext
    {
        public DbSet<Customer> Customers { get; set; }
        private string _config;

        //TODO For migration in entity Framework
        //public MyDbContext()
        //{
        //    _config = "Host = HOSTNAM; Port = PORT; Database = DATABASE; Username = USERNAME; Password = PASSWORD";
        //    Database.EnsureCreated();
        //}

        public MyDbContext(string config)
        {
            _config = config;
            Database.EnsureCreated();
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseNpgsql(_config);
        }
    }
}
