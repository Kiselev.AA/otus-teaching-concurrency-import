﻿using System;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;


namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Generator", "defoult.csv");
        private static int _numberRows = 10000;
        private static string _generation = "P";
        private static int _numberThreads = 10;
        private static readonly string _generatorPath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Generator", "Otus.Teaching.Concurrency.Import.DataGenerator.App.exe");
        private static string _dbConfig = "defoult db configuration";
        static void Main(string[] args)
        {
            if (!TryValidateAndParseArgs(args))
                return;
            
            Console.WriteLine($"Loader is started with parameters: \n" +
                    $"FileName:\t{_dataFileName}\n" +
                    $"NumberRows:\t{_numberRows}\n" +
                    $"Generate:\t{_generation}\n" +
                    $"Threads:\t{_numberThreads}\n" +
                    $"Process ID:\t{Process.GetCurrentProcess().Id}\n");

            var timesBegin = DateTime.Now;

            var starterGenerator = new StarterGenerator(_generatorPath, _dataFileName, _numberRows, _generation);
            starterGenerator.Start();

            Console.WriteLine();
            if (_generation == "P")
                starterGenerator.Wait();
            var loader = new MyDataLoader(_dataFileName, _numberThreads, _dbConfig);
            loader.LoadData();
            Console.WriteLine();
            Console.WriteLine($"Время выполнения: {DateTime.Now - timesBegin}");
        }

        private static bool TryValidateAndParseArgs(string[] args)
        {
            bool result = true;
            if (args != null && args.Length > 0)
            {
                _dataFileName = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, $"{args[0]}.xml");
                if (args.Length > 1)
                {
                    int.TryParse(args[1], out _numberRows);
                    if ((args.Length > 2) && (args[2].Length==1) && ("MPmp".Contains(args[2])))
                    {
                        _generation = args[2].ToUpper();
                        if (args.Length > 3)
                        {
                            int.TryParse(args[3], out _numberThreads);
                        }
                    }
                }
            }
            return result;
        }

    }
}