﻿using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using System;
using System.Diagnostics;
using System.IO;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    public  class StarterGenerator
    {
        private Process process;
        public string GeneratorProgramName { get; set; }
        public string OutputFileName { get; set; }
        public int NumbersRows { get; set; }
        public string GenerationType { get; set; }

        public StarterGenerator(string generatorProgramName, string outputFileName, int numbersRows, string generationType)
        {
            GeneratorProgramName = generatorProgramName;
            OutputFileName = outputFileName;
            NumbersRows = numbersRows;
            GenerationType = generationType;
        }

        public void Start()
        {
            if (GenerationType == "M")
            {
                StartGeneratorInCurrentProcess();
            }
            else if (GenerationType=="P")
            {
                StartGeneratorInNewProcess();
            }
            else
            {
                throw new Exception("Error Generation Type!");
            }
        }

        public void Wait()
        {
            process.WaitForExit();
        }

        private void StartGeneratorInNewProcess()
        {
            process = new Process();
            process.StartInfo.FileName = GeneratorProgramName;
            process.StartInfo.Arguments = "\"" + Path.GetFileNameWithoutExtension(OutputFileName) + "\" " + NumbersRows.ToString();
            process.Start();
            Console.WriteLine($"Generation started with process Id {process.Id}...");
        }

        private void StartGeneratorInCurrentProcess()
        {
            Console.WriteLine($"Generation started with process Id {Process.GetCurrentProcess().Id}...");
            var xmlGenerator = new CsvGenerator(OutputFileName, NumbersRows);
            xmlGenerator.Generate();
        }
    }
}
