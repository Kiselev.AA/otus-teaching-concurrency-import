﻿using Otus.Teaching.Concurrency.Import.DataAccess;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
    internal class MyDataLoader : IDataLoader
    {
        private readonly CustomerRepository repository;
        private readonly List<string> ReadData;
        private readonly int numberThread;
        private List<Customer> customers;

        public MyDataLoader(string fileName, int numberThread, string dbConfig)
        {
            repository = new CustomerRepository(new MyDbContext(dbConfig));
            var parser = new CsvParser();
            ReadData = parser.ReadFile(fileName);
            this.numberThread = numberThread;
        }

        public void LoadData()
        {
            var stopWatch = new Stopwatch();

            Console.WriteLine($"Start load without thread...");
            stopWatch.Start();
            ParseNotThread();
            stopWatch.Stop();
            Console.WriteLine($"Load without thread finish after {stopWatch.Elapsed} second.");
            Console.WriteLine();

            Console.WriteLine($"Start load with thread...");
            stopWatch = new Stopwatch();
            stopWatch.Start();
            ParseWithThread(numberThread);
            stopWatch.Stop();
            Console.WriteLine($"Load with thread finish after {stopWatch.Elapsed} second.");
            Console.WriteLine();

            Console.WriteLine($"Start load with threadpool...");
            stopWatch = new Stopwatch();
            stopWatch.Start();
            ParseWithThreadpool(numberThread);
            stopWatch.Stop();
            Console.WriteLine($"Load with threadpool finish after {stopWatch.Elapsed} second.");
            Console.WriteLine();
        }

        private void ParseNotThread()
        {
            repository.Clear();
            customers = new List<Customer>();

            var parser = new CsvParser(ReadData);
            customers = parser.Parse();
            
            LoadListInRepository();
            repository.SaveChange();
        }

        private void ParseWithThread(int numberThreads)
        {
            repository.Clear();
            customers = new List<Customer>();
            var subDatas = GetListSubData(numberThreads, ReadData);
            var threads = new List<Thread>();
            var e = new CountdownEvent(numberThreads);

            for (int i = 0; i < numberThreads; i++)
            {
                var parser = new CsvParser(subDatas[i]);
                threads.Add(new Thread(() =>
                {
                    Parsing(parser,e);
                }));

                threads[i].Start();
            }

            e.Wait();

            LoadListInRepository();
            repository.SaveChange();
        }

        private void ParseWithThreadpool(int numberThreads)
        {
            repository.Clear();
            customers = new List<Customer>();
            var subDatas = GetListSubData(numberThreads, ReadData);

            var e = new CountdownEvent(numberThreads);
            for (int i = 0; i < numberThreads; i++)
            {
                var data=new CsvParser(subDatas[i]);
                ThreadPool.QueueUserWorkItem(new WaitCallback(x =>
                { Parsing(data,e); }), data);
            }
            e.Wait();
            
            LoadListInRepository();
            repository.SaveChange();
        }

        private void Parsing(CsvParser parser, CountdownEvent e)
        {
            var subCustomers=parser.Parse();
            foreach (var subCustomer in subCustomers)
            {
                customers.Add(subCustomer);
            }
            e.Signal();
        }

        private void LoadListInRepository()
        {
            foreach (var item in customers)
            {
                repository.AddCustomer(item);
            }
        }

        private List<List<string>> GetListSubData(int number, List<string> data)
        {
            var result=new List<List<string>>();

            for (int i=0; i<number; i++)
            {
                result.Add(new List<string>());
                result[i].Add(data[0]);
            }

            for (int i = 1; i < data.Count; i++)
            {
                result[i % number].Add(data[i]);
            }
            return result;
        }
    }
}
